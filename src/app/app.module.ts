import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Component } from '@angular/core';

import { AppComponent } from './app.component';
import { MemoComponent } from './memo/memo.component';

import { Router, Routes, RouterModule } from "@angular/router";
import { NotFoundComponent } from './not-found/not-found.component';
import { NavigationComponent } from './navigation/navigation.component';
import { LeftSideBarComponent } from './left-side-bar/left-side-bar.component';
import { CreateMemoComponent } from './create-memo/create-memo.component';

const appRoute: Routes = [
  {
    path: "memo",
    component: MemoComponent
  }
  ,{
    path: "",
    component: MemoComponent, 
    pathMatch:"full"
  },
  {
    path: "**",
    component: NotFoundComponent
  }
];


@NgModule({
  declarations: [
    AppComponent,
    MemoComponent,
    NavigationComponent,
    LeftSideBarComponent,
    CreateMemoComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoute)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

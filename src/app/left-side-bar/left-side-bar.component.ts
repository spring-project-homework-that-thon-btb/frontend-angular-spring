import { Component, OnInit } from '@angular/core';
import { $ } from 'protractor';

@Component({
  selector: 'app-left-side-bar',
  templateUrl: './left-side-bar.component.html',
  styleUrls: ['./left-side-bar.component.css']
})
export class LeftSideBarComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  fn_newNote(event) { 
    //just added console.log which will display the event details in browser on click of the button.
    alert("Button is clicked");    
    console.log(event);    
 }

}
